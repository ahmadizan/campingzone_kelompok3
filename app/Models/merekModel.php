<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class merekModel extends Model
{
    use HasFactory;

    protected $table = 'mereks';
    protected $fillable = [
        'name',
    ];
}
