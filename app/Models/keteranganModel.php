<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\kategoriModel;
use App\Models\barangModel;

class keteranganModel extends Model
{
    use HasFactory;

    protected $table = 'keterangans';
    protected $fillable = [
        'title', 'barang_id', 'kategori_id'
    ];

    public function band()
    {
        return $this->belongsTo(kategoriModel::class, 'kategori_id', 'id');
    }
    public function barang()
    {
        return $this->belongsTo(barangModel::class, 'barang_id', 'id');
    }
}
