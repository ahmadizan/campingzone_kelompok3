<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\merekModel;
use App\Models\keteranganModel;

class barangModel extends Model
{

    protected $table = 'barangs';
    protected $fillable = [
        'name', 'year', 'merek_id', 'price', 'cover'
    ];

    public function merek()
    {
        return $this->belongsTo(merekModel::class, 'merek_id', 'id');
    }

    public function keterangan()
    {
        return $this->hasMany(keteranganModel::class, 'barang_id', 'id');
    }
}
