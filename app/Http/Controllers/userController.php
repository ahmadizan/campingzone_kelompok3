<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class userController extends Controller
{
    public function home()
    {
        $data = [
            'active' => 'home'
        ];
        return view('home', $data);
    }

    public function edit()
    {
        $data = [
            'data' => Auth::user(),
            'active' => 'home'
        ];
        return view('editUser', $data);
    }

    public function auth()
    {
        $data = [
            'active' => 'home'
        ];
        return view('auth', $data);
    }

    public function update(Request $r)
    {
        $update = [
            'name' => $r->name,
            'email' => $r->email,
            'address' => $r->address,
            'gender' => $r->gender
        ];

        if ($r->pass != null) {
            $update['password'] = bcrypt($r->pass);
        }

        User::where('id', Auth::id())
            ->update($update);
        return redirect('home')->with('scs', 'Data berhasil diperbarui');
    }

    public function reg(Request $req)
    {
        User::create([
            'name' => $req->name,
            'email' => $req->email,
            'password' => bcrypt($req->pass),
            'address' => $req->address,
            'gender' => $req->gender,
        ]);
        return redirect('auth')->with('scs', 'Registrasi berhasil');
    }

    public function log(Request $req)
    {
        if (Auth::attempt([
            'email' => $req->email,
            'password' => $req->pass
        ])) {

            return redirect('home');
        } else {
            return redirect('auth')->with('scs', 'Proses masuk tidak berhasil');
        }
    }

    public function out()
    {
        request()->session()->flush();
        Auth::logout();
        return redirect('/');
    }
}
