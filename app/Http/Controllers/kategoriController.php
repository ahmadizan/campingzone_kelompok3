<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\kategoriModel;
use App\Models\keteranganModel;

class kategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'data' => kategoriModel::all(),
            'active' => 'kategori'
        ];
        return view('kategori', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        kategoriModel::create([
            'name' => $request->kategoriName,
        ]);

        return redirect('/kategori')->with('scs', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'data' => kategoriModel::find($id),
            'active' => 'kategori'
        ];
        return view('editKategori', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        kategoriModel::where('id', $id)
            ->update(['name' => $request->kategoriName]);
        return redirect('/kategori')->with('scs', 'Data berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (empty(keteranganModel::where('kategori_id', $id)->first())) {
            kategoriModel::find($id)->delete();
            return redirect('/kategori')->with('scs', 'Data berhasil dihapus');
        } else {
            return redirect('/kategori')->with('scs', 'Data masih digunakan pada keterangan');
        }
    }
}
