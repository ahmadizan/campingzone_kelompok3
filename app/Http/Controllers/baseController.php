<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Models\barangModel;

class baseController extends Controller
{
    public function index()
    {
        $data = [
            'active' => false,
            'barang' => barangModel::all()
        ];
        // dd(albumModel::all()[0]->song[0]->title);
        // dd(albumModel::all()[0]->song[0]->band->name);
        return view('sell', $data);
    }
}
