<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\merekModel;
use App\Models\barangModel;

class merekController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'data' => merekModel::all(),
            'active' => 'merek'
        ];
        return view('merek', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        merekModel::create([
            'name' => $request->merekName,
        ]);
        return redirect('/merek')->with('scs', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'data' => merekModel::find($id),
            'active' => 'merek'
        ];
        return view('editmerek', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        merekModel::where('id', $id)
            ->update(['name' => $request->merekName]);
        return redirect('/merek')->with('scs', 'Data berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (empty(barangModel::where('merek_id', $id)->first())) {
            merekModel::find($id)->delete();
            return redirect('/merek')->with('scs', 'Data berhasil dihapus');
        } else {
            return redirect('/merek')->with('scs', 'Data masih digunakan pada barang');
        }
    }
}
