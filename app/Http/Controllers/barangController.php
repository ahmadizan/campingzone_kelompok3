<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\merekModel;
use App\Models\barangModel;
use App\Models\kategoriModel;
use App\Models\keteranganModel;
use Illuminate\Support\Facades\Storage;

class barangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'merek' => merekModel::select(['id', 'name'])->get(),
            'kategori' => kategoriModel::select(['id', 'name'])->get(),
            'data' => barangModel::all(),
            'active' => 'barang'
        ];
        // dd($data['publisher'][0]['id']);
        // dd($data['data'][0]->publisher);

        return view('barang', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->file('cover'));

        // dd(Str::random(10));
        $str = Str::random(10);
        $extension = $request->cover->extension();

        $newName = $str . '.' . $extension;
        // dd($newName);

        $path = $request->cover->storeAs('public/images', $newName);

        barangModel::create([
            'name' => $request->name,
            'year' => $request->tahun,
            'merek_id' => $request->merek,
            'kategori_id' => $request->kategori,
            'price' => $request->harga,
            'cover' => $newName
        ]);
        return redirect('/barang')->with('scs', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'data' => barangModel::find($id),
            'merek' => merekModel::select(['id', 'name'])->get(),
            'kategori' => kategoriModel::select(['id', 'name'])->get(),
            'active' => 'barang'
        ];
        return view('editBarang', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->cover == null) {
            $update = [
                'name' => $request->name,
                'year' => $request->tahun,
                'merek_id' => $request->merek,
                'price' => $request->harga
            ];
        } else {
            Storage::delete('public/images/' . $request->lastcover);

            $str = Str::random(10);
            $extension = $request->cover->extension();

            $newName = $str . '.' . $extension;
            // dd($newName);

            $path = $request->cover->storeAs('public/images', $newName);

            $update = [
                'name' => $request->name,
                'year' => $request->tahun,
                'merek_id' => $request->merek,
                'price' => $request->harga,
                'cover' => $newName
            ];
        }
        barangModel::where('id', $id)
            ->update($update);
        return redirect('/barang')->with('scs', 'Data berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (empty(keteranganModel::where('barang_id', $id)->first())) {

            $img = barangModel::find($id)->cover;
            Storage::delete('public/images/' . $img);
            barangModel::find($id)->delete();
            return redirect('/barang')->with('scs', 'Data berhasil dihapus');
        } else {
            return redirect('/barang')->with('scs', 'Data masih digunakan pada keterangan');
        }
    }
}
