<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\barangModel;
use App\Models\kategoriModel;
use App\Models\keteranganModel;

class keteranganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'barang' => barangModel::select(['id', 'name'])->get(),
            'kategori' => kategoriModel::select(['id', 'name'])->get(),
            'data' => keteranganModel::all(),
            'active' => 'keterangan'
        ];
        return view('keterangan', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        keteranganModel::create([
            'title' => $request->name,
            'barang_id' => $request->barang,
            'kategori_id' => $request->kategori,
        ]);
        return redirect('/keterangan')->with('scs', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'barang' => barangModel::select(['id', 'name'])->get(),
            'kategori' => kategoriModel::select(['id', 'name'])->get(),
            'data' => keteranganModel::find($id),
            'active' => 'keterangan'
        ];
        // dd($data);
        return view('editKeterangan', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        keteranganModel::where('id', $id)
            ->update([
                'title' => $request->name,
                'barang_id' => $request->barang,
                'kategori_id' => $request->kategori,
            ]);
        return redirect('/keterangan')->with('scs', 'Data berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        keteranganModel::find($id)->delete();
        return redirect('/keterangan')->with('scs', 'Data berhasil dihapus');
    }
}
