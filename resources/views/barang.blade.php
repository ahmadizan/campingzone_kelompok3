@include('template.header')

<section id="band">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-4 py-5">
                <div class="card w-100">
                    <div class="card-body">
                        <h5 class="card-title">Tambah Barang</h5>

                        <form action="/barang" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="mb-3">
                                <label for="name" class="form-merek">Nama Barang</label>
                                <input type="text" class="form-control" name="name" id="name">
                            </div>
                            <div class="mb-3">
                                <label for="tahun" class="form-merek">Tahun</label>
                                <input type="number" class="form-control" name="tahun" id="tahun">
                            </div>
                            <div class="mb-3">
                                <label for="harga" class="form-merek">Harga</label>
                                <input type="number" class="form-control" name="harga" id="harga">
                            </div>
                            <div class="mb-3">
                                <label for="cover" class="form-merek">Foto</label>
                                <input type="file" class="form-control" name="cover" id="cover">
                            </div>
                            <div class="mb-3">
                                <label for="merek" class="form-merek">Merek</label>
                                <select class="form-select" id="merek" name="merek">
                                    <option selected>Open this select menu</option>
                                    @foreach ($merek as $l)
                                    <option value={{$l['id']}}>{{$l['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="submit" class="btn btn-success">Tambah</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="w-100 overflow-auto">

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Keterangan</th>
                                <th scope="col">Tahun</th>
                                <th scope="col">Harga</th>
                                <th scope="col">Foto</th>
                                <th scope="col">Merek</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $i = 0
                            @endphp
                            @foreach ($data as $d)
                            @php
                            $i += 1
                            @endphp
                            <tr>
                                <td scope="row">{{$i}}</td>
                                <td>{{$d->name}}</td>
                                <td>{{$d->year}}</td>
                                <td>{{$d->price}}</td>
                                <td><img src={{Storage::url('images/'.$d->cover)}} width="50px" height="50px"></td>
                                <td>{{$d->merek->name}}</td>
                                <td>
                                    <a href="/barang/edit/{{$d->id}}" class="btn btn-info">Edit</a>
                                    <a href="/barang/delete/{{$d->id}}" class="btn btn-danger">Hapus</a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</section>
@include('template.footer')