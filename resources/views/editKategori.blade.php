@include('template.header')
<section>
    <div class="container">
        <div class="row py-5">
            <div class="col-12 col-md-6 col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <h5>Edit Kategori</h5>
                        <form action="/kategori/update/{{$data['id']}}" method="post">
                            @csrf
                            <div class="mb-3">
                                <label for="kategoriName" class="form-merek">Kategori</label>
                                <input type="text" value="{{$data['name']}}" class="form-control" name="kategoriName" id="kategoriName">
                            </div>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
@include('template.footer')