@include('template.header')
{{-- {{dd(Auth::user())}} --}}
<div class="container">
    <div class="row py-5">
        <div class="col-12 col-md-6 col-lg-4">

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Register</h5>
                    <form action="/auth/reg" method="post">
                        @csrf
                        <div class="mb-3">
                            <label for="name" class="form-merek">Nama</label>
                            <input type="text" class="form-control" id="name" name="name" required>
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-merek">Email</label>
                            <input type="email" class="form-control" id="email" name="email" required>
                        </div>
                        <div class="mb-3">
                            <label for="password" class="form-merek">Password</label>
                            <input type="password" class="form-control" id="password" minlength="8" name="pass" required>
                        </div>
                        <div class="mb-3">
                            <label for="address" class="form-merek">Alamat</label>
                            <input type="text" class="form-control" id="address" name="address" required>
                        </div>
                        <div class="mb-3">
                            <label for="address" class="form-merek">Gender</label>
                            <select class="form-select" aria-label="Default select example" name="gender" required>
                                <option value="l">Laki-laki</option>
                                <option value="p">Perempuan</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary w-100">Daftar</button>
                    </form>
                </div>
            </div>

        </div>
        <div class="col-12 col-md-6 col-lg-4 mt-4 mt-md-0">


            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Login</h5>
                    <form action="/auth/log" method="post">
                        @csrf
                        <div class="mb-3">
                            <label for="email" class="form-merek">Email</label>
                            <input type="email" class="form-control" id="email" required name="email">
                        </div>
                        <div class="mb-3">
                            <label for="password" class="form-merek">Password</label>
                            <input type="password" class="form-control" id="password" minlength="8" required name="pass">
                        </div>
                        <button type="submit" class="btn btn-primary w-100">Masuk</button>
                    </form>
                </div>
            </div>

        </div>
    </div>

</div>

</div>
{{-- $table->string('name');
$table->string('email')->unique();
$table->timestamp('email_verified_at')->nullable();
$table->string('password');
$table->string('role');
$table->string('address');
$table->string('gender'); --}}
@include('template.footer')