@include('template.header')

<section id="band">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-4 py-5">
                <div class="card w-100">
                    <div class="card-body">
                        <h5 class="card-title">Tambah Kategori</h5>
                        <form action="/kategori" method="post">
                            @csrf
                            <div class="mb-3">
                                <label for="kategoriName" class="form-merek">Kategori</label>
                                <input type="text" class="form-control" name="kategoriName" id="kategoriName">
                            </div>
                            <button type="submit" class="btn btn-success">Tambah</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="w-100 overflow-auto">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Kategori</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $i = 0;
                            @endphp
                            @foreach ($data as $d)
                            <tr>
                                @php
                                $i+=1
                                @endphp
                                <td scope="row">{{$i}}</td>
                                <td>{{$d->name}}</td>
                                <td>
                                    <a href="/kategori/edit/{{$d->id}}" class="btn btn-info">Edit</a>
                                    <a href="/kategori/delete/{{$d->id}}" class="btn btn-danger">Hapus</a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</section>
@include('template.footer')