<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>CAMPINGZONE</title>
</head>

<body style="min-height: 100vh">

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <a class="navbar-brand fw-bold" href="/">CAMPINGZONE</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ms-auto">
                    @if (Auth::user())
                    <a class="nav-link {{$active == 'home' ? 'active' : ''}}" href="/home">Home</a>
                    <a class="nav-link {{$active == 'merek' ? 'active' : ''}}" href="/merek">Merek</a>
                    <a class="nav-link {{$active == 'kategori' ? 'active' : ''}}" href="/kategori">Kategori</a>
                    <a class="nav-link {{$active == 'barang' ? 'active' : ''}}" href="/barang">Barang</a>
                    <a class="nav-link {{$active == 'keterangan' ? 'active' : ''}}" href="/keterangan">Keterangan</a>
                    @else
                    <a class="nav-link {{$active == 'auth' ? 'active' : ''}}" href="/auth">Register/Login</a>
                    @endif
                </div>
            </div>
        </div>
    </nav>

    @if (session('scs'))
    <div class="container mt-3">
        <div class="row">
            <div class="col">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('scs') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
        </div>
    </div>
    @endif