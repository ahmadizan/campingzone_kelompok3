@include('template.header')

<section style="background: url('/asset/poster.jpeg'); background-size: cover ; background-position: center; height: 500px">
    <div class="container-fluid h-100">
        <div class="row justify-content-center align-items-center h-100">
            <div class="container-fluid tm-brand-container-outer">
                <div class="row">
                    <div class="col-12">
                        <!-- Logo Area -->
                        <!-- Black transparent bg -->
                        <div class="tm-bg-black-transparent  text-white tm-brand-container-inner">
                            <div class="tm-brand-container text-center">
                                <h1 class="tm-brand-name">CAMPINGZONE</h1>
                                <p class="tm-brand-description mb-0">Temukan alat camping anda</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row py-5">

            @foreach ($barang as $a)

            <div class="col-12 col-md-6 col-lg-3 mb-3">
                <div class="card w-100 shadow">
                    <img src={{Storage::url('images/'.$a->cover)}} class="card-img-top" alt="..." style="height : 200px; object-fit:cover; object-position:center;">
                    <div class="card-body position-relative">
                        <h5 class="card-title">{{$a->name}}</h5>
                        <span class=" ms-3 fst-italic">{{$a->year}} {{isset($a->keterangan[0]) ? ' - '.$a->keterangan[0]->band->name : ''}}</span>
                        <hr>
                        <p class="card-text text-secondary overflow-hidden" style="height: 25px">
                            @php
                            $i = 0
                            @endphp
                            @foreach ($a->keterangan as $s)
                            {{ $i==0 ? $s->title : ' - '.$s->title}}
                            @php
                            $i++
                            @endphp
                            @endforeach
                        </p>
                        <hr>
                        @php
                        $price = $a->price;
                        $name = "'".$a->name."'";
                        @endphp
                        <h6>Rp {{$price}},00</h6>
                        <a class="btn btn-primary w-100" onclick="addAmount({{$name}},{{$price}})">Beli</a>
                    </div>
                </div>
            </div>

            @endforeach

        </div>
    </div>

    <div class="position-fixed w-100 d-flex flex-column align-items-center justify-content-center" style="bottom: 20px; left:0; right:0">
        <div class="w-75 py-2 px-5 d-none rounded rounded-pill align-items-center mb-3 border border-dark bg-light shadow-lg" id="detail">
        </div>
        <div class="w-75 py-2 px-5 rounded rounded-pill align-items-center bg-secondary shadow-lg d-flex justify-content-between">
            <h4 class="text-white" id="amount">Rp 0,00</h4>
            <div class="d-flex align-items-center">
                <p class="m-0 p-0 d-inline-block d-none" id="scs-bayar">Pembayaran Berhasil</p>
                <a class="rounded rounded-pill btn btn-light" id="bayar">Bayar</a>
                <a class="text-light btn-link btn text-decoration-none" id="btnDetail">Keranjang</a>
            </div>
        </div>
    </div>

    <script>
        let amount = document.getElementById('amount');
        let detail = document.getElementById('detail');
        let btndetail = document.getElementById('btnDetail');
        let scsB = document.getElementById('scs-bayar');
        let bayar = document.getElementById('bayar');

        let am = 0;

        let rmv = (el, prc) => {
            am -= prc;
            amount.innerHTML = 'Rp ' + am + ',00';

            el.parentNode.remove();
        }

        btndetail.addEventListener('click', () => {
            detail.classList.toggle('d-none')
        })

        let delhover = (t) => {
            t.classList.add('bg-danger');
        }

        let newEl = (barang, _price) => {
            let el = document.createElement("div");
            let name = document.createElement("p");
            let price = document.createElement("p");
            let del = document.createElement("a");

            el.classList.add('w-100');
            el.classList.add('d-flex');
            el.classList.add('justify-content-between');
            name.classList.add('m-0');
            price.classList.add('m-0');
            del.classList.add('btn-link');
            del.classList.add('text-danger');

            name.innerHTML = barang;
            price.innerHTML = "Rp " + _price + ",00";
            del.innerHTML = "hapus";

            el.appendChild(name);
            el.appendChild(price);
            el.appendChild(del);

            del.setAttribute("onclick", "rmv(this," + _price + ")");
            detail.appendChild(el);
        }

        let addAmount = (barang, a) => {
            am += a;
            amount.innerHTML = 'Rp ' + am + ',00';

            newEl(barang, a);
        }

        bayar.addEventListener('click', () => {
            bayar.classList.add('d-none');
            scsB.classList.remove('d-none');
            scsB.classList.remove('d-none');
            amount.innerHTML = '';
            am = 0;
            detail.innerHTML = '';
        })
    </script>
</section>
<style>
    .tm-bg-black-transparent {
        background-color: rgba(0, 0, 0, 0.5);
    }

    .tm-brand-container-outer {
        width: 580px;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .tm-brand-container-inner {
        width: 580px;
        height: 120%;
    }

    .tm-brand-name {
        font-size: 3.8rem;
    }

    .tm-brand-description {
        font-size: 1.4rem;
    }
</style>
@include('template.footer')