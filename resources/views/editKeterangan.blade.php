@include('template.header')

<section id="band">
    <div class="container">
        <div class="row py-5">
            <div class="col-12 col-md-6 col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <h5>Edit Keterangan</h5>
                        <form action="/keterangan/update/{{$data->id}}" method="post">
                            @csrf
                            <div class="mb-3">
                                <label for="name" class="form-merek">Judul</label>
                                <input type="text" class="form-control" name="name" id="name" value={{$data->title}}>
                            </div>
                            <div class="mb-3">
                                <label for="barang" class="form-merek">Keterangan</label>
                                <select class="form-select" id="barang" name="barang">
                                    <option selected>Open this select menu</option>
                                    @foreach ($barang as $a)
                                    <option value={{$a['id']}} {{$a['id']==$data->barang_id ? 'selected':''}}>{{$a['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="kategori" class="form-merek">Kategori</label>
                                <select class="form-select" id="kategori" name="kategori">
                                    <option selected>Open this select menu</option>
                                    @foreach ($kategori as $b)
                                    <option value={{$b['id']}} {{$b['id']==$data->kategori_id ? 'selected':''}}>{{$b['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('template.footer')