@include('template.header')
{{-- {{dd(Auth::user())}} --}}
<div class="container">
    <div class="row py-5">
        <div class="col-12 col-md-6 col-lg-4">

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Edit Data</h5>
                    <form action="/user/update/" method="post">
                        @csrf
                        <div class="mb-3">
                            <label for="name" class="form-merek">Nama</label>
                            <input type="text" class="form-control" value={{$data->name}} id="name" name="name" required>
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-merek">Email</label>
                            <input type="email" class="form-control" id="email" value={{$data->email}} name="email" required>
                        </div>
                        <div class="mb-3">
                            <label for="password" class="form-merek">Password</label>
                            <input type="password" class="form-control" id="password" minlength="8" name="pass">
                        </div>
                        <div class="mb-3">
                            <label for="address" class="form-merek">Alamat</label>
                            <input type="text" class="form-control" value={{$data->address}} id="address" name="address" required>
                        </div>
                        <div class="mb-3">
                            <label for="address" class="form-merek">Gender</label>
                            <select class="form-select" aria-label="Default select example" name="gender" required>
                                <option value="l" {{ $data->gender=='l'?'selected':'' }}>Laki-laki</option>
                                <option value="p" {{ $data->gender=='p'?'selected':'' }}>Perempuan</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary w-100">Simpan</button>
                    </form>
                </div>
            </div>

        </div>
    </div>

</div>

</div>
@include('template.footer')