@include('template.header')

<section id="band">
    <div class="container">
        <div class="row py-5">
            <div class="col-12 col-md-6 col-lg-4">

                <div class="card">
                    <div class="card-body">
                        <h5>Edit Barang</h5>
                        <form action="/barang/update/{{$data->id}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" value={{$data->cover}} name="lastcover">
                            <div class="mb-3">
                                <label for="name" class="form-merek">Nama</label>
                                <input type="text" class="form-control" name="name" id="name" value={{$data->name}}>
                            </div>
                            <div class="mb-3">
                                <label for="tahun" class="form-merek">Tahun</label>
                                <input type="number" class="form-control" name="tahun" id="tahun" value={{$data->year}}>
                            </div>
                            <div class="mb-3">
                                <label for="harga" class="form-merek">Harga</label>
                                <input type="number" class="form-control" name="harga" id="harga" value={{$data->price}}>
                            </div>
                            <div class="mb-3">
                                <label for="cover" class="form-merek">Cover</label>
                                <input type="file" class="form-control" name="cover" id="cover">
                            </div>
                            <div class="mb-3">
                                <label for="merek" class="form-merek">merek</label>
                                <select class="form-select" id="merek" name="merek">
                                    @foreach ($merek as $l)
                                    <option value={{$l['id']}} {{ $data->merek_id==$l['id']?'selected':'' }}>{{$l['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
@include('template.footer')