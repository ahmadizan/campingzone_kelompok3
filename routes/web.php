<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\kategoriController;
use App\Http\Controllers\merekController;
use App\Http\Controllers\barangController;
use App\Http\Controllers\keteranganController;
use App\Http\Controllers\userController;
use App\Http\Controllers\baseController;

use App\Http\Middleware\loginCheck;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider withikategori which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [baseController::class, 'index']);

Route::prefix('auth')->group(function () {
    Route::get('/', [userController::class, 'auth']);
    Route::post('/reg', [userController::class, 'reg']);
    Route::post('/log', [userController::class, 'log']);
    Route::get('/out', [userController::class, 'out']);
});

Route::middleware(loginCheck::class)->group(function () {
    // Route::middleware()->group(function () {

    Route::prefix('user')->group(function () {
        Route::get('/edit', [userController::class, 'edit']);
        Route::post('/update', [userController::class, 'update']);
    });

    Route::get('/home', [userController::class, 'home']);

    Route::prefix('kategori')->group(function () {
        Route::get('/', [kategoriController::class, 'index']);
        Route::post('/', [kategoriController::class, 'store']);
        Route::get('/edit/{id}', [kategoriController::class, 'edit']);
        Route::post('/update/{id}', [kategoriController::class, 'update']);
        Route::get('/delete/{id}', [kategoriController::class, 'destroy']);
    });

    Route::prefix('merek')->group(function () {

        Route::get('/', [merekController::class, 'index']);
        Route::post('/', [merekController::class, 'store']);
        Route::get('/edit/{id}', [merekController::class, 'edit']);
        Route::post('/update/{id}', [merekController::class, 'update']);
        Route::get('/delete/{id}', [merekController::class, 'destroy']);
    });

    Route::prefix('barang')->group(function () {

        Route::get('/', [barangController::class, 'index']);
        Route::post('/', [barangController::class, 'store']);
        Route::get('/edit/{id}', [barangController::class, 'edit']);
        Route::post('/update/{id}', [barangController::class, 'update']);
        Route::get('/delete/{id}', [barangController::class, 'destroy']);
    });

    Route::prefix('keterangan')->group(function () {

        Route::get('/', [keteranganController::class, 'index']);
        Route::post('/', [keteranganController::class, 'store']);
        Route::get('/edit/{id}', [keteranganController::class, 'edit']);
        Route::post('/update/{id}', [keteranganController::class, 'update']);
        Route::get('/delete/{id}', [keteranganController::class, 'destroy']);
    });
});
